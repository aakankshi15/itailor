#!/bin/bash


echo What is your component name?
read COMPONENT_NAME


first=`echo $COMPONENT_NAME|cut -c1|tr [a-z] [A-Z]`
second=`echo $COMPONENT_NAME|cut -c2-`
#echo $first$second

echo "Your $COMPONENT_NAME will be created..."
mkdir src/app/$COMPONENT_NAME
echo "--Folder Created--"
touch src/app/$COMPONENT_NAME/$COMPONENT_NAME.component.html
cat > src/app/$COMPONENT_NAME/$COMPONENT_NAME.component.html << EOF1
<p>$COMPONENT_NAME works!</p>
EOF1
echo "--HTML Created--"
touch src/app/$COMPONENT_NAME/$COMPONENT_NAME.component.scss
echo "--SCSS Created--"
touch src/app/$COMPONENT_NAME/$COMPONENT_NAME.spec.ts
echo "--Spec Created--"
touch src/app/$COMPONENT_NAME/$COMPONENT_NAME.component.ts
cat > src/app/$COMPONENT_NAME/$COMPONENT_NAME.component.ts << EOF1
import { Component, OnInit } from '@angular/core';
 @Component(
 {
 selector: 'app-$COMPONENT_NAME',
 templateUrl: './$COMPONENT_NAME.component.html',
 styleUrls: ['./$COMPONENT_NAME.component.scss']
 })

 export class ${first}${second}Component implements OnInit {

  constructor() {}
  ngOnInit() { }

  }
EOF1
echo "--Component Created--"
touch src/app/$COMPONENT_NAME/$COMPONENT_NAME.module.ts
cat > src/app/$COMPONENT_NAME/$COMPONENT_NAME.module.ts << EOF1
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '@app/material.module';
import { ${first}${second}RoutingModule } from './${first}${second}-routing.module';
import { ${first}${second}Component  } from './${COMPONENT_NAME}.component';

@NgModule({
  imports: [CommonModule, FlexLayoutModule, MaterialModule, ${first}${second}RoutingModule],
  declarations: [${first}${second}Component]
})
export class ${first}${second}Module {}

EOF1
echo "--Module Created--"
touch src/app/$COMPONENT_NAME/$COMPONENT_NAME.routing.module.ts
cat > src/app/$COMPONENT_NAME/$COMPONENT_NAME.routing.module.ts << EOF1
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/core';
import { ${first}${second}Component } from './$COMPONENT_NAME.component';

const routes: Routes = [
  // Module is lazy loaded, see app-routing.module.ts
  { path: '', component: ${first}${second}Component, data: { title: extract('${first}${second}') } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class ${first}${second}RoutingModule {}

EOF1
echo "--Routing Module Created--"