import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { EditFitRoutingModule } from './edit-fit-routing.module';
import { EditFitComponent } from './edit-fit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [CommonModule, MaterialModule, TranslateModule, EditFitRoutingModule, FormsModule, ReactiveFormsModule],
  declarations: [EditFitComponent]
})
export class EditFitModule {}
