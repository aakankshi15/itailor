import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-fit',
  templateUrl: './edit-fit.component.html',
  styleUrls: ['./edit-fit.component.scss']
})
export class EditFitComponent implements OnInit {
  userToken: string;
  fit_id: any;
  details: any;

  constructor(
    private fb: FormBuilder,
    private https: HttpClient,
    private router: Router,
    private aroute: ActivatedRoute
  ) {
    this.userToken = localStorage.getItem('credentials');
    // this.fit_id = localStorage.getItem('fit_id');
  }

  EditFitForm = new FormGroup({
    name: new FormControl()
  });

  ngOnInit() {
    this.aroute.queryParams.subscribe(params => {
      this.fit_id = params['fit'];
    });
    this.getFit();
  }

  getFit() {
    const data = {
      fit_id: this.fit_id,
      user: this.userToken
    };
    this.https.post<any>('api/edit_fits', data).subscribe(response => {
      this.details = response.fit;
      this.EditFitForm.controls['name'].setValue(this.details.name);
    });
  }

  submitData() {
    const data = {
      fit_id: this.fit_id,
      name: this.EditFitForm.value.name,
      user: this.userToken
    };

    this.https.post<any>('api/fit/fitUpdate', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.router.navigate(['list-fit']);
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }
}
