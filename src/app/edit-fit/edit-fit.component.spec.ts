import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFitComponent } from './edit-fit.component';

describe('EditFitComponent', () => {
  let component: EditFitComponent;
  let fixture: ComponentFixture<EditFitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditFitComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
