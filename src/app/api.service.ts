import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class User {
  email: number;
  password: string;
}
export class ApiService {
  baseUrl: string = 'http://localhost:8080/user-portal/users';

  constructor(private http: HttpClient) {}
  createUser(user: User) {
    return this.http.post(this.baseUrl, user);
  }
}
