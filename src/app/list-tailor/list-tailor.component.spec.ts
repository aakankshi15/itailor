import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTailorComponent } from './list-tailor.component';

describe('ListTailorComponent', () => {
  let component: ListTailorComponent;
  let fixture: ComponentFixture<ListTailorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListTailorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTailorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
