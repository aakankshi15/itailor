import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-list-tailor',
  templateUrl: './list-tailor.component.html',
  styleUrls: ['./list-tailor.component.scss']
})
export class ListTailorComponent implements OnInit {
  displayedColumns: string[] = ['sno', 'name', 'phone', 'alt_phone', 'address', 'Action'];
  userToken: string;
  tailor: MatTableDataSource<any>;
  edit_id: any;
  view_id: any;
  ID: any;
  listComponentFlag: Boolean = true;
  tabComponentFlag: Boolean = true;

  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  constructor(private https: HttpClient, private router: Router) {
    this.userToken = localStorage.getItem('credentials');
  }

  ngOnInit() {
    this.getTailor();
    this.listComponentFlag = true;
  }

  onClick(id: any) {
    this.ID = id;
    this.tabComponentFlag = true;
    this.listComponentFlag = false;
  }
  getTailor() {
    const data = {
      user: this.userToken
    };

    this.https.post<any>('api/getailor', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.tailor = new MatTableDataSource(response.tailors);
          this.tailor.paginator = this.paginator;
          this.tailor.sort = this.sort;
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  delete(id: number) {
    alert('Are you sure! Want to delete this item?');
    const data = {
      id: id,
      user: this.userToken
    };
    this.https.post<any>('api/delete_tailor', data).subscribe(response => {
      this.getTailor();
    });
  }

  edit(id: number) {
    this.router.navigate(['/edit-fit'], { queryParams: { edit_id: id } });
  }

  view(id: number) {
    this.router.navigate(['/tailor-profile'], { queryParams: { view_id: id } });
  }

  applyFilter(filterValue: any) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.tailor.filter = filterValue;
    if (this.tailor.paginator) {
      this.tailor.paginator.firstPage();
    }
  }
}
