import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { ListTailorRoutingModule } from './list-tailor-routing.module';
import { ListTailorComponent } from './list-tailor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [CommonModule, TranslateModule, MaterialModule, ListTailorRoutingModule, FormsModule, ReactiveFormsModule],
  declarations: [ListTailorComponent]
})
export class ListTailorModule {}
