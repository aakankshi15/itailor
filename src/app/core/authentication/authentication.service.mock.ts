import { Observable, of } from 'rxjs';

import { Credentials, LoginContext } from './authentication.service';

export class MockAuthenticationService {
  credentials: Credentials | null = {
    name: 'test',
    token: '123'
  };
  //
  // login(context: LoginContext): Observable<Credentials> {
  //   return of({
  //     phone_number: context.password
  //   });
  // }

  logout(): Observable<boolean> {
    this.credentials = null;
    return of(true);
  }

  isAuthenticated(): boolean {
    return !!this.credentials;
  }
}
