import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {
  addCategoryForm: FormGroup;
  userToken: string;
  measurementData: Array<any> = [];
  measurementArray: Array<any> = [];
  item: any;

  constructor(private fb: FormBuilder, private https: HttpClient, private router: Router) {
    this.userToken = localStorage.getItem('credentials');
  }

  ngOnInit() {
    this.formValidator();
    this.getMeasurement();
  }

  formValidator() {
    this.addCategoryForm = this.fb.group({
      name: ['', Validators.required],
      categoryArray: this.fb.array([this.getData()])
    });
  }

  getData() {
    return this.fb.group({
      label: ['', [Validators.required]],
      measurements: this.fb.array([])
    });
  }

  addNew() {
    const control = <FormArray>this.addCategoryForm.controls['categoryArray'];
    control.push(this.getData());
  }

  removeLair(index: any) {
    this.categoryArray.removeAt(index);
  }

  get categoryArray(): FormArray {
    return this.addCategoryForm.get('categoryArray') as FormArray;
  }

  // onChange(index: number, _id: string, name: string, isChecked: boolean) {
  //   if (isChecked) {
  //     this.addCategoryForm.controls.categoryArray.value[index].measurements.push({ id: _id, name: name });
  //   } else {
  //     this.addCategoryForm.controls.categoryArray.value[index].measurements=this.addCategoryForm.controls.categoryArray.value[index].measurements.filter(item=>_id!==item.id);

  //   }
  // }
  onChange(index: number, data: { _id: string; name: string }, isChecked: boolean) {
    if (isChecked) {
      this.addCategoryForm.controls.categoryArray.value[index].measurements.push(data);
    } else {
      this.addCategoryForm.controls.categoryArray.value[index].measurements.splice(
        this.addCategoryForm.controls.categoryArray.value[index].measurements.indexOf(data),
        1
      );
    }
  }

  submitdata() {
    const data = {
      name: this.addCategoryForm.value.name,
      user: this.userToken,
      measurement: this.addCategoryForm.value.categoryArray
    };

    this.https.post<any>('api/categories', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.router.navigate(['list-category']);
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  getMeasurement() {
    const data = {
      user: this.userToken
    };
    this.https.post<any>('api/get_measurement', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.measurementData = response.measurements;
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }
}
