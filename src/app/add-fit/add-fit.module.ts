import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { AddFitRoutingModule } from './add-fit-routing.module';
import { AddFitComponent } from './add-fit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [CommonModule, TranslateModule, MaterialModule, AddFitRoutingModule, FormsModule, ReactiveFormsModule],
  declarations: [AddFitComponent]
})
export class AddFitModule {}
