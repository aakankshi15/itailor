import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-fit',
  templateUrl: './add-fit.component.html',
  styleUrls: ['./add-fit.component.scss']
})
export class AddFitComponent implements OnInit {
  addFitForm: FormGroup;
  userToken: string;

  constructor(private fb: FormBuilder, private https: HttpClient, private router: Router) {
    this.userToken = localStorage.getItem('credentials');
    this.formValue();
  }

  ngOnInit() {}

  submitdata() {
    const data = {
      name: this.addFitForm.value.name,
      user: this.userToken
    };

    this.https.post<any>('api/fits', data).subscribe(
      response => {
        if (response.code === 200) {
          this.router.navigate(['list-fit']);
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  formValue() {
    this.addFitForm = this.fb.group({
      name: ['', Validators.required]
    });
  }
}
