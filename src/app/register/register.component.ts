import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  RegisterForm: FormGroup;
  API_CALL: string;

  constructor(private fb: FormBuilder, private https: HttpClient, private router: Router) {}

  ngOnInit() {
    this.formvalue();
  }

  formvalue() {
    this.RegisterForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      name: ['', Validators.required],
      phone_number: ['', Validators.required]
    });
  }

  register() {
    this.https.post<any>('api/register', this.RegisterForm.value).subscribe(
      response => {
        if (response.response_code === 200) {
          localStorage.setItem('credentials', response.success.token);
          localStorage.setItem('name', response.success.name);
          this.router.navigate(['/'], { replaceUrl: true });
        }
      },
      credentials => {
        console.log(credentials);
        //this.router.navigate(['/'], { replaceUrl: true });
      }
    );
  }
}
