import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Global } from '../shared/global';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {
  userToken: string;
  view_id: any;
  orderData: any;
  measeureData: any;
  breakpoint: number;
  imgUrl = `${Global.webUrl}`;

  constructor(private https: HttpClient, private router: Router, private aroute: ActivatedRoute) {
    this.userToken = localStorage.getItem('credentials');
  }

  ngOnInit() {
    this.aroute.queryParams.subscribe(params => {
      this.view_id = params['order'];
      this.getOrder();
    });

    this.getOrder();
    (this.breakpoint = window.innerWidth <= 767 ? 2 : 4), window.innerWidth <= 480 ? 1 : 2;
  }

  onResize(event: any) {
    (this.breakpoint = event.target.innerWidth <= 767 ? 2 : 4), event.target.innerWidth <= 480 ? 1 : 2;
  }

  // view(id: number) {
  //   this.router.navigate(['/order-details'], { queryParams: { order: id } });
  // }

  getOrder() {
    const data = {
      // user: this.userToken,
      order_id: this.view_id
    };
    this.https.post<any>('api/view_order', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.orderData = response.orders;
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }
}
