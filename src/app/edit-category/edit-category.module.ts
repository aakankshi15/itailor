import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { EditCategoryRoutingModule } from './edit-category-routing.module';
import { EditCategoryComponent } from './edit-category.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [CommonModule, MaterialModule, TranslateModule, EditCategoryRoutingModule, FormsModule, ReactiveFormsModule],
  declarations: [EditCategoryComponent]
})
export class EditCategoryModule {}
