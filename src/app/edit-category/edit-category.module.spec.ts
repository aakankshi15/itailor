import { EditCategoryModule } from './edit-category.module';

describe('EditCategoryModule', () => {
  let editCategoryModule: EditCategoryModule;

  beforeEach(() => {
    editCategoryModule = new EditCategoryModule();
  });

  it('should create an instance', () => {
    expect(editCategoryModule).toBeTruthy();
  });
});
