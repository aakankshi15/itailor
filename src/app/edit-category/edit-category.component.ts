import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit {
  editCategoryForm: FormGroup;
  userToken: string;
  category_id: any;
  category: any;
  measurementData: Array<any> = [];
  temp1: any;
  item: any;

  constructor(
    private fb: FormBuilder,
    private https: HttpClient,
    private router: Router,
    private aroute: ActivatedRoute
  ) {
    this.userToken = localStorage.getItem('credentials');
  }

  ngOnInit() {
    this.aroute.queryParams.subscribe(params => {
      this.category_id = params['category'];
    });

    this.formValidator();
    this.getMeasurement();
    this.getCategory();
  }

  getCategory() {
    const data = {
      category_id: this.category_id,
      user: this.userToken
    };

    this.https.post<any>('api/categoryById', data).subscribe(response => {
      this.category = response.categories;
      this.editCategoryForm.controls['name'].setValue(this.category.name);

      for (let i = 0; i < this.category.measurement.length; i++) {
        if (i !== 0) {
          const control = <FormArray>this.editCategoryForm.controls['categoryArray'];
          control.push(this.getData());
        }
        this.categoryArray
          .at(i)
          .get('label')
          .setValue(this.category.measurement[i].label);
        this.categoryArray
          .at(i)
          .get('id')
          .setValue(this.category.measurement[i].id);

        for (let j = 0; j < this.category.measurement[i].measurements.length; j++) {
          let temp1 = this.category.measurement[i].measurements[j];
          this.editCategoryForm.controls.categoryArray.value[i].measurements.push({ id: temp1.id, name: temp1.name });
        }
      }
    });
  }

  inputChecked(i: any, data: any) {
    let checked = false;
    for (let l = 0; l < this.category.measurement[i].measurements.length; l++) {
      let temp = this.category.measurement[i].measurements[l];
      if (temp.name == data.name && temp.id == data.id) {
        checked = true;
      }
    }
    return checked;
  }

  formValidator() {
    this.editCategoryForm = this.fb.group({
      name: ['', Validators.required],
      categoryArray: this.fb.array([this.getData()])
    });
  }

  getData() {
    return this.fb.group({
      label: ['', [Validators.required]],
      id: [''],
      measurements: this.fb.array([])
    });
  }

  addNew() {
    const control = <FormArray>this.editCategoryForm.controls['categoryArray'];
    control.push(this.getData());
  }

  removeLair(index: any) {
    this.categoryArray.removeAt(index);
  }

  get categoryArray(): FormArray {
    return this.editCategoryForm.get('categoryArray') as FormArray;
  }

  onChange(index: number, _id: string, name: string, isChecked: boolean) {
    if (isChecked) {
      this.editCategoryForm.controls.categoryArray.value[index].measurements.push({ id: _id, name: name });
    } else {
      this.editCategoryForm.controls.categoryArray.value[
        index
      ].measurements = this.editCategoryForm.controls.categoryArray.value[index].measurements.filter(
        (item: any) => _id !== item.id
      );
    }
  }

  getMeasurement() {
    const data = {
      user: this.userToken
    };
    this.https.post<any>('api/get_measurement', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.measurementData = response.measurements;
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  submitdata() {
    const data = {
      category_id: this.category_id,
      name: this.editCategoryForm.value.name,
      measurements: this.editCategoryForm.value.categoryArray
    };
    this.https.post<any>('api/update_categories', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.router.navigate(['list-category']);
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }
}
