import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Global } from '../shared/global';

@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.scss']
})
export class EditOrderComponent implements OnInit {
  userToken: string;
  view_id: any;
  details: any;
  fitData: Array<any> = [];
  tailorData: Array<any> = [];
  measurementId: string;
  categoryData: Array<any> = [];
  clickCategoryData: Array<any> = [];
  measure_data: Array<any> = [];
  price: any;
  advancePrice: any;
  balancePrice: any;
  fabPhoto: File;
  desiPhoto: File;
  minDate = new Date();
  cloth_material: any;
  imgUrl = `${Global.webUrl}`;

  constructor(
    private fb: FormBuilder,
    private https: HttpClient,
    private router: Router,
    private aroute: ActivatedRoute
  ) {
    this.userToken = localStorage.getItem('credentials');
    // this.view_id = localStorage.getItem('view_id');
    this.formValue();
  }

  public measurementType = [{ value: 'Inch' }, { value: 'Cm' }];
  public fabricMaterial = [{ value: 'Provided' }, { value: 'Not-Provided' }];

  EditOrderForm = new FormGroup({
    measurement_type: new FormControl(),
    fit_type: new FormControl(),
    category: new FormControl(),
    tailor: new FormControl(null),
    customer_phone_number: new FormControl(),
    customer_name: new FormControl(),
    completion_date: new FormControl(),
    order_date: new FormControl(),
    order_taken_by: new FormControl(),
    price: new FormControl(),
    advance: new FormControl(),
    balance: new FormControl(),
    cloth_material: new FormControl(),
    cloth_measurements: new FormControl(),
    febric_photo: new FormControl(),
    design_photo: new FormControl(),
    remarks: new FormControl()
  });

  ngOnInit() {
    this.aroute.queryParams.subscribe(params => {
      this.view_id = params['order'];
    });
    this.getorderData();
  }
  ngAfterContentInit() {
    this.getFitType();
    this.getTailor();
    this.getCategory();
  }

  getCategory() {
    const data = {
      user: this.userToken
    };
    this.https.post<any>('api/get_categories', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.categoryData = response.categories;
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  getTailor() {
    const data = {
      user: this.userToken
    };
    this.https.post<any>('api/getailor', data).subscribe(response => {
      if (response.response_code === 200) {
        this.tailorData = response.tailors;
      }
    });
  }

  categoryOnSelect(id: string) {
    this.measurementId = id;
    // this.getCategoryData();
  }

  getFitType() {
    const data = {
      user: this.userToken
    };
    this.https.post<any>('api/get_fits', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.fitData = response.fit;
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  // getCategoryData = () => {
  //   this.https
  //     .post<any>('api/get_measurement', {
  //       user: this.userToken,
  //       measurement_id: this.measurementId
  //     })
  //     .subscribe(
  //       response => {
  //         if (response.response_code === 200) {
  //           this.clickCategoryData = response.categories;
  //           // this.clickCategoryData.forEach(data => {
  //           //   let mesureArray: Array<any> = [];
  //           //   data.measurements.forEach((mes: any) => {
  //           //     mesureArray.push({
  //           //       name: mes.name,
  //           //       value: ''
  //           //     });
  //           //   });
  //           //   data.measurements = mesureArray;
  //           // });
  //         }
  //       },
  //       error => {
  //         console.log(error.error);
  //       }
  //     );
  // };

  getorderData() {
    const data = {
      order_id: this.view_id
      // user: this.userToken
    };
    this.https.post<any>('api/view_order', data).subscribe(response => {
      this.details = response.orders;
      this.EditOrderForm.controls['measurement_type'].setValue(this.details.measurement_in);
      this.EditOrderForm.controls['fit_type'].setValue(this.details.fit_size);
      this.EditOrderForm.controls['category'].setValue(this.details.category);
      this.EditOrderForm.controls['tailor'].setValue(this.details.tailor);
      this.EditOrderForm.controls['customer_phone_number'].setValue(this.details.phone_no);
      this.EditOrderForm.controls['customer_name'].setValue(this.details.user_name);
      this.EditOrderForm.controls['completion_date'].setValue(this.details.completation_date);
      this.EditOrderForm.controls['order_date'].setValue(this.details.order_date);
      this.EditOrderForm.controls['order_taken_by'].setValue(this.details.order_taken_by);
      this.EditOrderForm.controls['price'].setValue(this.details.price);
      this.EditOrderForm.controls['advance'].setValue(this.details.advance);
      this.EditOrderForm.controls['balance'].setValue(this.details.balance);
      this.EditOrderForm.controls['cloth_material'].setValue(this.details.cloth_material);
      this.EditOrderForm.controls['cloth_measurements'].setValue(this.details.cloth_measurement);
      this.EditOrderForm.controls['remarks'].setValue(this.details.remarks);
    });
  }

  formValue() {
    this.EditOrderForm = this.fb.group({
      // order_taken_by: new FormControl({ value: '', disabled: true }, Validators.required),
      // customer_phone_number: new FormControl({ value: '', disabled: true }, Validators.required),
      // customer_name: new FormControl({ value: '', disabled: true }, Validators.required),
      order_taken_by: ['', Validators.required],
      customer_phone_number: ['', Validators.required],
      customer_name: ['', Validators.required],
      category: ['', Validators.required],
      fit_type: ['', Validators.required],
      tailor: ['', Validators.required],
      measurements: ['', Validators.required],
      measurement_type: ['', Validators.required],

      completion_date: ['', Validators.required],
      order_date: ['', Validators.required],

      remarks: ['', Validators.required],
      price: ['', Validators.required],
      advance: ['', Validators.required],
      balance: ['', Validators.required],
      cloth_material: ['', Validators.required],
      cloth_measurements: ['', Validators.required],
      id: ['', Validators.required]
    });
  }
  submitData() {
    const data = {
      order_id: this.view_id,
      category: this.EditOrderForm.value.category,
      category_id: this.details.category_id,
      fit_size: this.EditOrderForm.value.fit_type,
      tailor: this.EditOrderForm.value.tailor,
      measurements: this.details.measure_data,
      measurement_in: this.EditOrderForm.value.measurement_type,
      phone_no: this.EditOrderForm.value.customer_phone_number,
      user_name: this.EditOrderForm.value.customer_name,
      completation_date: this.EditOrderForm.value.completion_date,
      order_date: this.EditOrderForm.value.order_date,
      order_taken_by: this.EditOrderForm.value.order_taken_by,
      remarks: this.EditOrderForm.value.remarks,
      price: this.EditOrderForm.value.price,
      advance: this.EditOrderForm.value.advance,
      balance: this.EditOrderForm.value.balance,
      cloth_material: this.EditOrderForm.value.cloth_material,
      cloth_measurement: this.EditOrderForm.value.cloth_measurements,
      user: this.userToken
    };
    this.https.post<any>('api/update_orders', data).subscribe(
      response => {
        if (response.response_code === 200) {
          // this.router.navigate(['list-order']);
          alert('Order Successfully Updated');
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  febricPhoto(event: any) {
    this.fabPhoto = <File>event.target.files[0];
  }
  designPhoto(event: any) {
    this.desiPhoto = <File>event.target.files[0];
  }

  submitMedia() {
    var data = new FormData();
    {
      data.append('febric_photo', this.fabPhoto);
      data.append('design_photo', this.desiPhoto);
      data.append('order_id', this.view_id);
    }
    this.https.post<any>('api/orderImage', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.router.navigate(['list-order']);
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  PriceFunction(e: any) {
    this.price = e.target.value;
  }

  advancePriceFunction(e: any) {
    this.advancePrice = e.target.value;
    this.balancePrice = this.price - this.advancePrice;
    this.EditOrderForm.controls['balance'].setValue(this.balancePrice);
  }
}
