import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { EditOrderRoutingModule } from './edit-order-routing.module';
import { EditOrderComponent } from './edit-order.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [CommonModule, MaterialModule, TranslateModule, EditOrderRoutingModule, FormsModule, ReactiveFormsModule],
  declarations: [EditOrderComponent]
})
export class EditOrderModule {}
