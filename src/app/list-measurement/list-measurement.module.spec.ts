import { ListMeasurementModule } from './list-measurement.module';

describe('ListMeasurementModule', () => {
  let listMeasurementModule: ListMeasurementModule;

  beforeEach(() => {
    listMeasurementModule = new ListMeasurementModule();
  });

  it('should create an instance', () => {
    expect(listMeasurementModule).toBeTruthy();
  });
});
