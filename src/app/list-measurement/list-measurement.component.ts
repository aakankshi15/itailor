import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-measurement',
  templateUrl: './list-measurement.component.html',
  styleUrls: ['./list-measurement.component.scss']
})
export class ListMeasurementComponent implements OnInit {
  userToken: string;
  measurementData: Array<any> = [];

  constructor(private https: HttpClient, private router: Router) {
    this.userToken = localStorage.getItem('credentials');
  }

  ngOnInit() {
    this.getMeasurement();
  }

  getMeasurement() {
    const data = {
      user: this.userToken
    };
    this.https.post<any>('api/get_measurement', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.measurementData = response.measurements;
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  delete(id: number) {
    alert('Are you sure! Want to delete measurement item?');
    const data = {
      id: id,
      user: this.userToken
    };
    this.https.post<any>('api/delete_measurement', data).subscribe(response => {
      this.getMeasurement();
    });
  }
}
