import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { ListMeasurementRoutingModule } from './list-measurement-routing.module';
import { ListMeasurementComponent } from './list-measurement.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    MaterialModule,
    ListMeasurementRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ListMeasurementComponent]
})
export class ListMeasurementModule {}
