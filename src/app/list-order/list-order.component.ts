import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-list-order',
  templateUrl: './list-order.component.html',
  styleUrls: ['./list-order.component.scss']
})
export class ListOrderComponent implements OnInit {
  displayedColumns: string[] = [
    'checkbox',
    'Order_Id',
    'Customer_Number',
    'Tailor',
    'taken_by',
    'Product_Name',
    'Status',
    'Action'
  ];
  userToken: string;
  orderData: MatTableDataSource<any>;
  view_id: any;
  checkedOrderId: Array<any> = [];
  listComponentFlag: Boolean = true;
  tabComponentFlag: Boolean = true;
  ID: any;
  checkedList: any;

  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  constructor(private https: HttpClient, private router: Router) {
    this.userToken = localStorage.getItem('credentials');
  }

  public OrderStatus = [{ value: 'Pending' }, { value: 'InProcess' }, { value: 'Completed' }];

  ngOnInit() {
    this.getOrder();
    this.listComponentFlag = true;
  }

  getOrder() {
    const data = {
      user: this.userToken
    };
    this.https.post<any>('api/get_orders', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.orderData = new MatTableDataSource(response.orders);
          this.orderData.paginator = this.paginator;
          this.orderData.sort = this.sort;
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  onClick(id: any) {
    this.ID = id;
    this.tabComponentFlag = true;
    this.listComponentFlag = false;
  }

  view(id: number) {
    this.router.navigate(['/order-details'], { queryParams: { order: id } });
  }

  editOrder(id: number) {
    this.router.navigate(['/edit-order'], { queryParams: { order: id } });
  }

  chkSelection(e: any) {
    if (e.target.checked == true) {
      this.checkedOrderId.push(e.target.value);
      console.log(this.checkedOrderId);
    } else {
      const index = this.checkedOrderId.indexOf(e.target.value);
      if (index > -1) {
        this.checkedOrderId.splice(index, 1);
      }
    }
  }

  onChangeStatus(e: any) {
    if (this.checkedOrderId.length > 0) {
      const data = {
        order_status: e.value,
        order_id: this.checkedOrderId
        // user: this.userToken
      };
      this.https.post<any>('api/updateStatus', data).subscribe(
        response => {
          if (response.response_code === 200) {
            this.getOrder();
          }
        },
        error => {
          console.log(error.error);
        }
      );
    } else {
      console.log('Check at least one');
    }
  }

  deleteOrder(id: number) {
    alert('Are you sure! Want to delete this item?');
    const data = {
      order_id: id
      // user: this.userToken
    };
    this.https.post<any>('api/delete_orders', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.getOrder();
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  applyFilter(filterValue: any) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.orderData.filter = filterValue;
    if (this.orderData.paginator) {
      this.orderData.paginator.firstPage();
    }
  }
}
