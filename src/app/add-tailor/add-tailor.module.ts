import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { AddTailorRoutingModule } from './add-tailor-routing.module';
import { AddTailorComponent } from './add-tailor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [CommonModule, MaterialModule, TranslateModule, AddTailorRoutingModule, FormsModule, ReactiveFormsModule],
  declarations: [AddTailorComponent]
})
export class AddTailorModule {}
