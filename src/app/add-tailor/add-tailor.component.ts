import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-tailor',
  templateUrl: './add-tailor.component.html',
  styleUrls: ['./add-tailor.component.scss']
})
export class AddTailorComponent implements OnInit {
  addTailorForm: FormGroup;
  userToken: string;
  ourFile: File = null;
  data1: any;

  constructor(private fb: FormBuilder, private https: HttpClient, private router: Router) {
    this.userToken = localStorage.getItem('credentials');
  }

  ngOnInit() {
    this.formValue();
  }

  formValue() {
    this.addTailorForm = this.fb.group({
      name: ['', Validators.required],
      phone_number: ['', Validators.required],
      alternate_number: ['', Validators.required],
      address: ['', Validators.required],
      profile: ['']
    });
  }

  onSelectFile(event: { target: { files: File[] } }) {
    this.ourFile = <File>event.target.files[0];
  }

  submitdata() {
    // const data = {
    //   name: this.addTailorForm.value.name,
    //   phone_number: this.addTailorForm.value.phone_number,
    //   alternate_number: this.addTailorForm.value.alternate_number,
    //   address: this.addTailorForm.value.address,
    //   // profile: this.data1,
    //   user: this.userToken
    // };
    var data = new FormData();
    {
      data.append('name', this.addTailorForm.value.name);
      data.append('phone_number', this.addTailorForm.value.phone_number);
      data.append('alternate_number', this.addTailorForm.value.alternate_number);
      data.append('address', this.addTailorForm.value.address);
      data.append('profile', this.ourFile);
      data.append('user', this.userToken);
    }

    this.https.post<any>('api/tailor', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.router.navigate(['list-tailor']);
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }
}
