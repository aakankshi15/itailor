import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { AddMeasurementRoutingModule } from './add-measurement-routing.module';
import { AddMeasurementComponent } from './add-measurement.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    MaterialModule,
    AddMeasurementRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [AddMeasurementComponent]
})
export class AddMeasurementModule {}
