import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-measurement',
  templateUrl: './add-measurement.component.html',
  styleUrls: ['./add-measurement.component.scss']
})
export class AddMeasurementComponent implements OnInit {
  addMeasurementForm: FormGroup;
  userToken: string;

  constructor(private fb: FormBuilder, private https: HttpClient, private router: Router) {
    this.userToken = localStorage.getItem('credentials');
    this.formValue();
  }

  ngOnInit() {}

  submitdata() {
    const data = {
      name: this.addMeasurementForm.value.name,
      user: this.userToken
    };
    this.https.post<any>('api/measurement', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.router.navigate(['list-measurement']);
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  formValue() {
    this.addMeasurementForm = this.fb.group({
      name: ['', Validators.required]
    });
  }
}
