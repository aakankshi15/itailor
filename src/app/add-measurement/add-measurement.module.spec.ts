import { AddMeasurementModule } from './add-measurement.module';

describe('AddMeasurementModule', () => {
  let addMeasurementModule: AddMeasurementModule;

  beforeEach(() => {
    addMeasurementModule = new AddMeasurementModule();
  });

  it('should create an instance', () => {
    expect(addMeasurementModule).toBeTruthy();
  });
});
