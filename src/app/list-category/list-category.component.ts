import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.scss']
})
export class ListCategoryComponent implements OnInit {
  userToken: string;
  categoryData: Array<any> = [];
  category_id: any;

  constructor(private https: HttpClient, private router: Router) {
    this.userToken = localStorage.getItem('credentials');
  }

  ngOnInit() {
    this.getCategory();
  }

  getCategory() {
    const data = {
      user: this.userToken
    };

    this.https.post<any>('api/get_categories', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.categoryData = response.categories;
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  delete(id: number) {
    alert('Are you sure! Want to delete category item?');
    const data = {
      id: id,
      user: this.userToken
    };
    this.https.post<any>('api/delete_categories', data).subscribe(response => {
      this.getCategory();
    });
  }

  // edit(id: number) {
  //   this.category_id = id;
  //   localStorage.setItem('category_id', this.category_id);
  //   this.router.navigate(['/update_categories']);
  // }

  edit(id: number) {
    this.router.navigate(['/edit-category'], { queryParams: { category: id } });
  }
}
