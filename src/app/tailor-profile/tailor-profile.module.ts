import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { TailorProfileRoutingModule } from './tailor-profile-routing.module';
import { TailorProfileComponent } from './tailor-profile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    MaterialModule,
    TailorProfileRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [TailorProfileComponent]
})
export class TailorProfileModule {}
