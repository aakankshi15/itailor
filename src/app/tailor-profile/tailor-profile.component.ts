import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Global } from '../shared/global';

@Component({
  selector: 'app-tailor-profile',
  templateUrl: './tailor-profile.component.html',
  styleUrls: ['./tailor-profile.component.scss']
})
export class TailorProfileComponent implements OnInit {
  userToken: string;
  edit_id: any;
  tailor: any;
  imgUrl = `${Global.webUrl}`;

  constructor(private https: HttpClient, private router: Router, private aroute: ActivatedRoute) {
    this.userToken = localStorage.getItem('credentials');
  }

  ngOnInit() {
    this.aroute.queryParams.subscribe(params => {
      this.edit_id = params['profile'];
      this.getTailor();
    });
  }

  getTailor() {
    const data = {
      tailor_id: this.edit_id,
      user: this.userToken
    };
    this.https.post<any>('api/edit_tailor', data).subscribe(response => {
      this.tailor = response.tailor;
    });
  }
}
