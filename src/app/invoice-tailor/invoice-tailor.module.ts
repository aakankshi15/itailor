import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { InvoiceTailorRoutingModule } from './invoice-tailor-routing.module';
import { InvoiceTailorComponent } from './invoice-tailor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    TranslateModule,
    InvoiceTailorRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [InvoiceTailorComponent]
})
export class InvoiceTailorModule {}
