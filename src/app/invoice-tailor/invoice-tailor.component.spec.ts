import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceTailorComponent } from './invoice-tailor.component';

describe('InvoiceTailorComponent', () => {
  let component: InvoiceTailorComponent;
  let fixture: ComponentFixture<InvoiceTailorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvoiceTailorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceTailorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
