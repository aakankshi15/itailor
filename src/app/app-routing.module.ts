import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { Shell } from '@app/shell/shell.service';

const routes: Routes = [
  Shell.childRoutes([{ path: 'about', loadChildren: 'app/about/about.module#AboutModule' }]),
  Shell.childRoutes([{ path: 'fab-photo', loadChildren: 'app/fabphoto/fabphoto.module#FabphotoModule' }]),
  Shell.childRoutes([{ path: 'place-order', loadChildren: 'app/place-order/place-order.module#PlaceOrderModule' }]),
  Shell.childRoutes([
    { path: 'tailor-profile', loadChildren: 'app/tailor-profile/tailor-profile.module#TailorProfileModule' }
  ]),

  Shell.childRoutes([
    { path: 'body-size', loadChildren: 'app/measurements/body-size/body-size.module#BodySizeModule' }
  ]),
  Shell.childRoutes([
    { path: 'standard-size', loadChildren: 'app/measurements/standard-size/standard-size.module#StandardSizeModule' }
  ]),
  Shell.childRoutes([{ path: 'measure', loadChildren: 'app/measurements/measure/measure.module#MeasureModule' }]),
  Shell.childRoutes([{ path: 'add-tailor', loadChildren: 'app/add-tailor/add-tailor.module#AddTailorModule' }]),
  Shell.childRoutes([{ path: 'list-tailor', loadChildren: 'app/list-tailor/list-tailor.module#ListTailorModule' }]),
  Shell.childRoutes([{ path: 'edit-tailor', loadChildren: 'app/edit-tailor/edit-tailor.module#EditTailorModule' }]),

  Shell.childRoutes([
    { path: 'add-measurement', loadChildren: 'app/add-measurement/add-measurement.module#AddMeasurementModule' }
  ]),
  Shell.childRoutes([
    { path: 'list-measurement', loadChildren: 'app/list-measurement/list-measurement.module#ListMeasurementModule' }
  ]),
  Shell.childRoutes([{ path: 'add-category', loadChildren: 'app/add-category/add-category.module#AddCategoryModule' }]),
  Shell.childRoutes([
    { path: 'list-category', loadChildren: 'app/list-category/list-category.module#ListCategoryModule' }
  ]),
  Shell.childRoutes([{ path: 'add-fit', loadChildren: 'app/add-fit/add-fit.module#AddFitModule' }]),
  Shell.childRoutes([{ path: 'list-fit', loadChildren: 'app/list-fit/list-fit.module#ListFitModule' }]),
  Shell.childRoutes([{ path: 'edit-fit', loadChildren: 'app/edit-fit/edit-fit.module#EditFitModule' }]),
  Shell.childRoutes([{ path: 'list-order', loadChildren: 'app/list-order/list-order.module#ListOrderModule' }]),
  Shell.childRoutes([
    { path: 'edit-category', loadChildren: 'app/edit-category/edit-category.module#EditCategoryModule' }
  ]),
  Shell.childRoutes([
    { path: 'order-details', loadChildren: 'app/order-details/order-details.module#OrderDetailsModule' }
  ]),
  Shell.childRoutes([{ path: 'edit-order', loadChildren: 'app/edit-order/edit-order.module#EditOrderModule' }]),
  Shell.childRoutes([
    { path: 'customer-invoice', loadChildren: 'app/invoice-customer/invoice-customer.module#InvoiceCustomerModule' }
  ]),
  Shell.childRoutes([
    { path: 'tailor-invoice', loadChildren: 'app/invoice-tailor/invoice-tailor.module#InvoiceTailorModule' }
  ]),
  // Fallback when no prior route is matched

  { path: 'register', loadChildren: 'app/register/register.module#RegisterModule' },
  { path: 'home', loadChildren: 'app/home/home.module#HomeModule' },
  { path: '**', redirectTo: '/', pathMatch: 'full' },
  { path: '', redirectTo: 'home', pathMatch: 'prefix' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
