import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { EditTailorRoutingModule } from './edit-tailor-routing.module';
import { EditTailorComponent } from './edit-tailor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, MaterialModule, TranslateModule, EditTailorRoutingModule, FormsModule, ReactiveFormsModule],
  declarations: [EditTailorComponent]
})
export class EditTailorModule {}
