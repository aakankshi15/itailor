import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTailorComponent } from './edit-tailor.component';

describe('EditTailorComponent', () => {
  let component: EditTailorComponent;
  let fixture: ComponentFixture<EditTailorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditTailorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTailorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
