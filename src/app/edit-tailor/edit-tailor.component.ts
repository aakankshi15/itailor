import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-tailor',
  templateUrl: './edit-tailor.component.html',
  styleUrls: ['./edit-tailor.component.scss']
})
export class EditTailorComponent implements OnInit {
  userToken: string;
  edit_id: any;
  tailor: any;

  constructor(
    private fb: FormBuilder,
    private https: HttpClient,
    private router: Router,
    private aroute: ActivatedRoute
  ) {
    this.userToken = localStorage.getItem('credentials');
    // this.edit_id = localStorage.getItem('edit_id');
    this.formValue();
  }
  editTailorForm = new FormGroup({
    name: new FormControl(),
    phone_number: new FormControl(),
    alternate_number: new FormControl(),
    address: new FormControl()
  });

  ngOnInit() {
    this.aroute.queryParams.subscribe(params => {
      this.edit_id = params['tailor'];
      this.getTailor();
    });

    // this.getTailor();
  }

  getTailor() {
    const data = {
      tailor_id: this.edit_id,
      user: this.userToken
    };
    this.https.post<any>('api/edit_tailor', data).subscribe(response => {
      this.tailor = response.tailor;
      this.editTailorForm.controls['name'].setValue(this.tailor.name);
      this.editTailorForm.controls['phone_number'].setValue(this.tailor.phone_number);
      this.editTailorForm.controls['alternate_number'].setValue(this.tailor.alternate_number);
      this.editTailorForm.controls['address'].setValue(this.tailor.address);
    });
  }

  submitdata() {
    const data = {
      tailor_id: this.edit_id,
      name: this.editTailorForm.value.name,
      phone_number: this.editTailorForm.value.phone_number,
      alternate_number: this.editTailorForm.value.alternate_number,
      address: this.editTailorForm.value.address,
      user: this.userToken
    };

    this.https.post<any>('api/update_tailor', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.router.navigate(['list-tailor']);
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  formValue() {
    this.editTailorForm = this.fb.group({
      name: ['', Validators.required],
      phone_number: ['', Validators.required],
      alternate_number: ['', Validators.required],
      address: ['', Validators.required],
      _id: ['', Validators.required]
    });
  }
}
