import { Component, AfterContentInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { MatGridList } from '@angular/material';

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.scss']
})
export class PlaceOrderComponent implements AfterContentInit {
  objectKeys = Object.keys;
  addOrderForm: FormGroup;
  userToken: string;
  userName: string;
  measurement_type: any;
  categoryData: Array<any> = [];
  // clickCategoryData: Array<any> = [];
  clickCategoryData: any;
  fitData: Array<any> = [];
  fitObject: Array<any> = [];
  tailorData: Array<any> = [];
  measurementId: string;
  orderForm: FormGroup;
  orderMedia: FormGroup;
  price: any;
  OrderId: any;
  advancePrice: any;
  balancePrice: any;
  minDate = new Date();
  cloth_material: any;

  @ViewChild('grid')
  grid: MatGridList;

  gridByBreakpoint = {
    // xl: 8,
    // lg: 6,
    lg: 4,
    md: 2,
    sm: 2,
    xs: 1
  };

  public measurementType = [{ value: 'Inch' }, { value: 'Cm' }];
  public fabricMaterial = [{ value: 'Provided' }, { value: 'Not-Provided' }];

  constructor(
    private fb: FormBuilder,
    private https: HttpClient,
    private router: Router,
    private observableMedia: ObservableMedia
  ) {
    this.userToken = localStorage.getItem('credentials');
    this.userName = localStorage.getItem('name');
  }

  ngAfterContentInit() {
    this.getCategory();
    this.getFitType();
    this.generateOrderForm();
    this.getTailor();
    this.setUser();
    this.observableMedia.asObservable().subscribe((change: MediaChange) => {
      this.grid.cols = this.gridByBreakpoint[change.mqAlias];
    });
  }

  form = new FormGroup({
    first: new FormControl({ value: 'Nancy', disabled: true }, Validators.required),
    last: new FormControl('Drew', Validators.required)
  });

  generateOrderForm() {
    this.orderForm = this.fb.group({
      measurement_type: [1],
      fit_type: [1],
      category: [],
      tailor: [''],
      customer_phone_number: ['', Validators.required],
      customer_name: ['', Validators.required],
      completion_date: ['', Validators.required],
      order_date: ['', Validators.required],
      order_taken_by: ['', Validators.required],
      price: ['', Validators.required],
      advance: ['', Validators.required],
      balance: ['', Validators.required],
      cloth_material: [],
      cloth_measurements: [],
      remarks: ['']
    });
  }

  submitData() {
    const data = {
      category: this.orderForm.value.category,
      category_id: this.measurementId,
      fit_size: this.orderForm.value.fit_type,
      tailor: this.orderForm.value.tailor,
      measurements: this.clickCategoryData,
      measurement_in: this.orderForm.value.measurement_type,
      phone_no: this.orderForm.value.customer_phone_number,
      user_name: this.orderForm.value.customer_name,
      completation_date: this.orderForm.value.completion_date,
      order_date: this.orderForm.value.order_date,
      order_taken_by: this.orderForm.value.order_taken_by,
      remarks: this.orderForm.value.remarks,
      price: this.orderForm.value.price,
      advance: this.orderForm.value.advance,
      balance: this.orderForm.value.balance,
      cloth_material: this.orderForm.value.cloth_material,
      cloth_measurement: this.orderForm.value.cloth_measurements
    };
    this.https.post<any>('api/addOrders', data).subscribe(
      response => {
        if (response.response_code === 200) {
          // this.router.navigate(['list-order']);
          this.OrderId = response.order_id;
          this.router.navigate(['/fab-photo'], { queryParams: { fabID: this.OrderId } });
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  // submitMedia() {
  //   var data = new FormData();
  //     {
  //       data.append('febric_photo', this.fabPhoto);
  //       data.append('design_photo', this.desiPhoto);
  //       data.append('order_id', this.OrderId);
  //     }

  //   this.https.post<any>('api/orderImage', data).subscribe(
  //     response => {
  //       if (response.response_code === 200) {
  //         this.router.navigate(['list-order']);
  //       }
  //     },
  //     error => {
  //       console.log(error.error);
  //     }
  //   );
  // };

  // get measurments
  getCategoryData = () => {
    this.https
      .post<any>('api/get_measureById', {
        user: this.userToken,
        category_id: this.measurementId
      })
      .subscribe(
        response => {
          if (response.response_code === 200) {
            this.clickCategoryData = response.categories;
            // this.clickCategoryData.forEach(data => {
            //   let mesureArray: Array<any> = [];
            //   data.measurement.forEach((mes: any) => {
            //     mesureArray.push({
            //       name: mes.label,
            //       value: ''
            //     });
            //   });
            //   data.measurements = mesureArray;
            // });
          }
        },
        error => {
          console.log(error.error);
        }
      );
  };

  getCategory() {
    const data = {
      user: this.userToken
    };
    this.https.post<any>('api/get_categories', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.categoryData = response.categories;
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  getTailor() {
    const data = {
      user: this.userToken
    };
    this.https.post<any>('api/getailor', data).subscribe(response => {
      if (response.response_code === 200) {
        this.tailorData = response.tailors;
      }
    });
  }

  categoryOnSelect(id: string) {
    this.measurementId = id;
    this.getCategoryData();
  }

  getFitType() {
    const data = {
      user: this.userToken
    };
    this.https.post<any>('api/get_fits', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.fitData = response.fit;
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  PriceFunction(e: any) {
    this.price = e.target.value;
  }

  setUser() {
    this.orderForm.controls['order_taken_by'].setValue(this.userName);
  }

  advancePriceFunction(e: any) {
    this.advancePrice = e.target.value;
    this.balancePrice = this.price - this.advancePrice;
    this.orderForm.controls['balance'].setValue(this.balancePrice);
  }

  getUser(e: any) {
    const data = {
      phone_no: e.target.value
    };
    this.https.post<any>('api/getUserById', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.orderForm.controls['customer_name'].setValue(response.user_name);
          // this.orderForm.controls['customer_name'].setValue(response.user[0].name);
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }
}
