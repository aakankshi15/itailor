import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-fit',
  templateUrl: './list-fit.component.html',
  styleUrls: ['./list-fit.component.scss']
})
export class ListFitComponent implements OnInit {
  userToken: string;
  fitData: Array<any> = [];
  fit_id: any;

  constructor(private https: HttpClient, private router: Router) {
    this.userToken = localStorage.getItem('credentials');
  }

  ngOnInit() {
    this.getFitType();
  }

  getFitType() {
    const data = {
      user: this.userToken
    };
    this.https.post<any>('api/get_fits', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.fitData = response.fit;
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  editfit(id: number) {
    this.router.navigate(['/edit-fit'], { queryParams: { fit: id } });

    // this.fit_id = id;
    // localStorage.setItem('fit_id', this.fit_id);
    // this.router.navigate(['/edit-fit']);
  }

  // view(id: number) {
  //   this.router.navigate(['/page-content'], { queryParams: { menu_id: id } });

  // }

  delete(id: number) {
    alert('Are you sure! Want to delete fit item?');
    const data = {
      fit_id: id,
      user: this.userToken
    };
    this.https.post<any>('api/delete_fits', data).subscribe(response => {
      this.getFitType();
    });
  }
}
