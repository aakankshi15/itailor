import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListFitComponent } from './list-fit.component';

describe('ListFitComponent', () => {
  let component: ListFitComponent;
  let fixture: ComponentFixture<ListFitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListFitComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListFitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
