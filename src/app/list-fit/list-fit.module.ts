import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { ListFitRoutingModule } from './list-fit-routing.module';
import { ListFitComponent } from './list-fit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [CommonModule, TranslateModule, MaterialModule, ListFitRoutingModule, FormsModule, ReactiveFormsModule],
  declarations: [ListFitComponent]
})
export class ListFitModule {}
