import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { InvoiceCustomerRoutingModule } from './invoice-customer-routing.module';
import { InvoiceCustomerComponent } from './invoice-customer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    TranslateModule,
    InvoiceCustomerRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [InvoiceCustomerComponent]
})
export class InvoiceCustomerModule {}
