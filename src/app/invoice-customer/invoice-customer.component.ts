import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Global } from '../shared/global';

@Component({
  selector: 'app-invoice-customer',
  templateUrl: './invoice-customer.component.html',
  styleUrls: ['./invoice-customer.component.scss']
})
export class InvoiceCustomerComponent implements OnInit {
  userToken: string;
  view_id: any;
  orderData: any;
  imgUrl = `${Global.webUrl}`;

  constructor(private https: HttpClient, private router: Router, private aroute: ActivatedRoute) {
    this.userToken = localStorage.getItem('credentials');
  }

  ngOnInit() {
    this.aroute.queryParams.subscribe(params => {
      this.view_id = params['order'];
      this.getOrder();
    });
  }

  getOrder() {
    const data = {
      order_id: this.view_id
    };
    this.https.post<any>('api/view_order', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.orderData = response.orders;
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }

  print() {
    window.print();
  }
}
