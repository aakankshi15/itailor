import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { MeasureRoutingModule } from './measure-routing.module';
import { MeasureComponent } from './measure.component';
@NgModule({
  imports: [CommonModule, TranslateModule, MaterialModule, MeasureRoutingModule],
  declarations: [MeasureComponent]
})
export class MeasureModule {}
