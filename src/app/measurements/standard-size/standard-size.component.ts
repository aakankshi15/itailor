import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { Global} from './../../shared/global';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-standard-size',
  templateUrl: './standard-size.component.html',
  styleUrls: ['./standard-size.component.scss']
})
export class StandardSizeComponent implements OnInit {
  selectionForm: FormGroup;
  Size: string;
  API_CALL: string;

  constructor(private fb: FormBuilder, private https: HttpClient, private router: Router) {}

  size = [
    { value: 'European Size', viewvalue: 'European Size' },
    { value: 'Uk/American Size', viewvalue: 'Uk/American Size' }
  ];

  ngOnInit() {
    this.formdata();
  }

  formdata() {
    this.selectionForm = this.fb.group({
      Size: ['', Validators.required]
      // measure_type: ['', Validators.required],
      // chest: ['', Validators.required],
      // stomach: ['', Validators.required],
      // hip: ['', Validators.required],
      // shoulder: ['', Validators.required],
      // sleeve: ['', Validators.required],
      // length: ['', Validators.required],

      // waist: ['', Validators.required],
      // hip:new FormControl(),
      // crotch: ['', Validators.required],
      // thigh: ['', Validators.required],
      // length:new FormControl(),
    });
  }

  submitdata() {
    this.https.post<any>(this.API_CALL, this.selectionForm.value).subscribe(
      response => {
        if (response.code === 200) {
          this.router.navigate(['company-success']);
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }
}
