import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { StandardSizeRoutingModule } from './standard-size-routing.module';
import { StandardSizeComponent } from './standard-size.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [CommonModule, TranslateModule, MaterialModule, StandardSizeRoutingModule, FormsModule, ReactiveFormsModule],
  declarations: [StandardSizeComponent]
})
export class StandardSizeModule {}
