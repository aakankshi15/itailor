import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { BodySizeRoutingModule } from './body-size-routing.module';
import { BodySizeComponent } from './body-size.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [CommonModule, MaterialModule, TranslateModule, BodySizeRoutingModule, FormsModule, ReactiveFormsModule],
  declarations: [BodySizeComponent]
})
export class BodySizeModule {}
