import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-body-size',
  templateUrl: './body-size.component.html',
  styleUrls: ['./body-size.component.scss']
})
export class BodySizeComponent implements OnInit {
  selectionForm: FormGroup;
  measure_type: string;
  fit: string;
  API_CALL: string;

  constructor(private fb: FormBuilder, private https: HttpClient, private router: Router) {}

  public measure = [{ value: 'Inch', viewvalue: 'Inch' }, { value: 'Cm', viewvalue: 'Cm' }];
  public Fit = [
    { value: 'Signature Standard Fit', viewvalue: 'Signature Standard Fit' },
    { value: 'Euro Slim Fit', viewvalue: 'Euro Slim Fit' }
  ];

  ngOnInit() {
    this.measure_type = this.measure[0].value;
    this.formvalue();
  }

  formvalue() {
    this.selectionForm = this.fb.group({
      measure_type: ['', Validators.required],
      chest: ['', Validators.required],
      stomach: ['', Validators.required],
      hip: ['', Validators.required],
      shoulder: ['', Validators.required],
      sleeve: ['', Validators.required],
      length: ['', Validators.required],

      waist: ['', Validators.required],
      // hip:new FormControl(),
      crotch: ['', Validators.required],
      thigh: ['', Validators.required],
      fit: ['', Validators.required]
      // length:new FormControl(),
    });
  }

  submitdata() {
    this.https.post<any>(this.API_CALL, this.selectionForm.value).subscribe(
      response => {
        if (response.code === 200) {
          this.router.navigate(['company-success']);
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }
}
