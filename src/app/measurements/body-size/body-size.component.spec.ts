import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodySizeComponent } from './body-size.component';

describe('BodySizeComponent', () => {
  let component: BodySizeComponent;
  let fixture: ComponentFixture<BodySizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BodySizeComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodySizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
