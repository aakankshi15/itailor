import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/material.module';
import { FabphotoRoutingModule } from './fabphoto-routing.module';
import { FabphotoComponent } from './fabphoto.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, MaterialModule, TranslateModule, FabphotoRoutingModule, FormsModule, ReactiveFormsModule],
  declarations: [FabphotoComponent]
})
export class FabphotoModule {}
