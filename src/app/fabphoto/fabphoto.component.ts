import { Component, AfterContentInit, ViewChild, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-fabphoto',
  templateUrl: './fabphoto.component.html',
  styleUrls: ['./fabphoto.component.scss']
})
export class FabphotoComponent implements OnInit {
  userToken: string;
  fabPhoto: File;
  desiPhoto: File;
  OrderId: any;

  constructor(private https: HttpClient, private router: Router, private aroute: ActivatedRoute) {
    this.userToken = localStorage.getItem('credentials');
  }

  ngOnInit() {
    this.aroute.queryParams.subscribe(params => {
      this.OrderId = params['fabID'];
    });
  }

  febricPhoto(event: any) {
    this.fabPhoto = <File>event.target.files[0];
  }
  designPhoto(event: any) {
    this.desiPhoto = <File>event.target.files[0];
  }

  submitMedia() {
    var data = new FormData();
    {
      data.append('febric_photo', this.fabPhoto);
      data.append('design_photo', this.desiPhoto);
      data.append('order_id', this.OrderId);
    }
    this.https.post<any>('api/orderImage', data).subscribe(
      response => {
        if (response.response_code === 200) {
          this.router.navigate(['list-order']);
        }
      },
      error => {
        console.log(error.error);
      }
    );
  }
}
