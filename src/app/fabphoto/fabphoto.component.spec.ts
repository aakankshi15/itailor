import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FabphotoComponent } from './fabphoto.component';

describe('FabphotoComponent', () => {
  let component: FabphotoComponent;
  let fixture: ComponentFixture<FabphotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FabphotoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FabphotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
